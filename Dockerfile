# Imagen raíz
FROM node
# Carpeta raíz
WORKDIR /apitechu
# Copia de archivos desde la carpeta en que estoy (directorio ".") a destino ("/apitechu")
ADD . /apitechu
# Instalo los paquetes necesarios
RUN npm install
# Voluen de la imagen
VOLUME ["/logs"]
# Puerto que expone la imagen
EXPOSE 3000
# Comando de iniciado
CMD ["npm", "start"]
