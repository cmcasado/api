/**
 * Preparamos las variables de la aplicación
 */

//instancio EXPRESS que es un servidor http
var express = require('express')
//instancio BodyParser
var bodyParser = require('body-parser')
//instancio el servicio FS
var fs = require('fs')
//instancio el objeto request-json
var requestJson = require ('request-json')
//instancio mi aplicación
var app = express()
//le digo a mi aplicación que use el bodyParser
app.use(bodyParser.json())

//declaramos las variables para conectar a la BBDD
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancocmc/collections"
var apiKey = 'apiKey=xeZ2t3S9_cxroaQUliesxA-pKSerJyux'
var clienteMlab = null

//creo una variable puerto que va a estar escuchand en la variable de entorno PORT o en el 3000 por defecto
var port = process.env.PORT || 3000

//le digo a mi app que quiero escuchar en el puerto port
app.listen(port)
console.log("API escuchando en el puerto "+port)


/**
 * Desarrollamos las funcionalidades de la api de usuarios
 */
 //me cargo el fichero de usuarios en una variable
 var usuarios = require('./usuarios.json')

 /**
  * Comenzamos a implementar los servicios de los recursos
  */

 //con esto le dices, a partir de localhost, qué quieres que te enseñe desde la ruta que pones en el primer parámetro
 app.get('/apitechu/v1', function(req, res){
   //console.log(req)
   //el SEND debería ser lo último de la función siempre
   res.send({"mensaje": "Bienvenido a mi API"})
 })

 app.get('/apitechu/v1/usuarios', function(req, res){
   //el SEND debería ser lo último de la función siempre
   //res.sendfile("usuarios.json")
   res.send(usuarios)
 })

/**
 * Devolver lista de usuarios a partir de Mlab
 */
 app.get('/apitechu/v5/usuarios', function(req, res){
   //instancio la variable con la petición que voy a realizar a Mlab
   clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + apiKey)
   //hago la petición a la API de Mlab
   clienteMlab.get('', function(err, resM, body){
     if(!err){
       //si todo ha ido bien, devuelvo la lista de usuarios que está en body
       res.send(body)
     }
     else{
       res.send("Error: "+ err)
     }
   })
 })

 /**
  * Devolver Nombre y Apellido de un usuario a partir de Mlab
  */
  app.get('/apitechu/v5/usuarios/:id', function(req, res){
    //recojo el id a consultar
    var id = req.params.id
    //construcción query para obtener solo nombre y apellido
    //le pongo l=1 para que solo me devuelva 1 ya que por diseño he garantizado que el id es único
    var query = 'q={"id":'+id+'}&f={"nombre":1, "apellido":1, "_id":0}&l=1&'
    //instancio la variable con la petición que voy a realizar a Mlab
    clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + query + apiKey)
    //hago la petición a la API de Mlab
    clienteMlab.get('', function(err, resM, body){
      if(!err){
        //si tiene elementos devuelve el primero
        if (body.length > 0){
          //si todo ha ido bien, devuelvo body
          res.send(body[0])
        }
        else{
          res.status(404).send("Usuario no encontrado")
        }
      }
      else{
        res.send()
      }
    })
  })

 app.post('/apitechu/v1/usuarios', function(req, res){
   //console.log(req.headers)
   var nuevo = {"first_name":req.headers.first_name,
               "country":req.headers.country}
   //usuarios es un array, por eso lo inserta con push
   usuarios.push(nuevo)
   //guardamos el resultado del fichero
   const datos = JSON.stringify(usuarios)
   fs.writeFile("./usuarios.json", datos, "utf8", function(err){
     if (err){
       console.log(err)
     }
     else{
       console.log("Fichero guardado")
     }
   })

   //siempre es recomendable devolver un 'send'
   res.send("Alta ok")
 })

 app.post('/apitechu/v2/usuarios', function(req, res){
   var nuevo = req.body
   //usuarios es un array, por eso lo inserta con push
   usuarios.push(nuevo)
   //guardamos el resultado del fichero
   const datos = JSON.stringify(usuarios)
   fs.writeFile("./usuarios.json", datos, "utf8", function(err){
     if (err){
       console.log(err)
     }
     else{
       console.log("Fichero guardado")
     }
   })

   //siempre es recomendable devolver un 'send'
   res.send("Alta ok")
 })

 //con ":id" le digo a js que es un parámetro
 app.delete('/apitechu/v1/usuarios/:id', function(req, res){
   //el método splice es, a partir del índice que yo te paso, cuántos elementos eliminas
   //entonces, si le paso el id = 100, borraré la posición 99 del array
   usuarios.splice(req.params.id-1, 1)

   res.send("Usuario borrado")
 })

 /**
  * Ver los parámetros recibidos por consola
  */
 app.post('/apitechu/v1/monstruo/:p1/:p2', function(req, res){
   console.log("Parámetros")
   console.log(req.params)
   console.log("Query strings")
   console.log(req.query)
   console.log("Headers")
   console.log(req.headers)
   console.log("Body")
   console.log(req.body)

   res.send("Monstruo ejecutado")
 })

 /**
  * LOGIN
  */
 app.post('/apitechu/v1/login', function(req, res){

   //recuperamos de la cabecera el usuario y el password
   var usuario = req.headers.usuario
   var password = req.headers.password
   //mantenemos el id del usuario logueado
   var idUsuario = 0
   //inicializamos la posición del array del usuario logueado a -1 para que no devuelva ningún usuario si no lo encuentra
   var posIdUsuario = -1

   //recorremos la colección elemento a elemento mientras no haya login
   for (var i = 0; i < usuarios.length; i++) {
     //console.log(usuarios[i].email)

     //comprobamos que coincida el email y el password
     if (usuarios[i].email == usuario && usuarios[i].password == password) {
       //activamos el atributo logged
       usuarios[i].logged = true
       //guardamos el idUsuario
       idUsuario = usuarios[i].id
       //guardamos la posición en la que está el usuario logueado
       posIdUsuario = i
       //si encuentra el ID deja de buscar
       break;
     }
   }

   //si hemos podido hacer login, se devuelve un mensaje con el usuario logueado
   if (idUsuario != 0) {
     //devolvemos un JSON
     res.send({"login": usuarios[posIdUsuario].logged, "ID": idUsuario, "posicionIdUsuario": posIdUsuario})
   }
   //si no, devolvemos usuario no identificado
   else{
     //devolvemos un JSON
     res.send({"login": "false"})
   }
 })

 /**
  * LOGIN con Mlab
  */
  app.post('/apitechu/v5/login', function(req, res){

    //recuperamos de la cabecera el usuario y el password
    var email = req.headers.email
    var password = req.headers.password

    //construcción query para recuperar si existe un usuario con el email y el password facilitados
    //le pongo l=1 para que solo me devuelva 1 ya que por diseño he garantizado que el email es único
    var query = 'q={"email":'+'"'+email+'"'+', "password":'+'"'+password+'"'+'}&f={"id":1, "nombre":1, "apellido":1, "_id":0}&l=1&'

    //instancio la variable con la petición que voy a realizar a Mlab
    clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + query + apiKey)

    //hago la petición a la API de Mlab
    clienteMlab.get('', function(errM, resM, body){
      if(!errM){
        //si he hecho login
        if (body.length == 1){
          //ahora quiero actualizar el atributo logged en la BBDD para el cliente que ha logueado
          clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?q={"id":'+body[0].id+'}&'+apiKey)
          //seteamos el atributo a modificar
          var cambio = '{"$set":{"logged":true}}'

          //realizamos la llamada PUT que va a actualizar el documento
          clienteMlab.put('', JSON.parse(cambio), function(errP, resP, bodyP){
            if(!errP){
              //al usuario le devuelvo por pantalla el resultado
              res.send({"Login":"OK", "id":body[0].id, "nombre":body[0].nombre, "apellido":body[0].apellido})
            }
            else{
              res.end(errP)
            }
          })
        }
        else{
          res.status(404).send("Usuario no encontrado")
        }
      }
      else{
        res.send()
      }
    })
  })

/**
 * LOGOUT con Mlab
 */
  app.post('/apitechu/v5/logout', function(req, res){

    //recuperamos de la cabecera el ID
    var id = req.headers.id

    //construcción query para recuperar si hay un ID que haya hecho login previamente
    //le pongo l=1 para que solo me devuelva 1 ya que por diseño he garantizado que el email es único
    var query = 'q={"id":'+id+', "logged":true}&f={"id":1, "nombre":1, "apellido":1, "_id":0}&l=1&'

    //instancio la variable con la petición que voy a realizar a Mlab
    clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + query + apiKey)

    //hago la petición a la API de Mlab
    clienteMlab.get('', function(errM, resM, body){
      if(!errM){
        //si estaba logueado
        if (body.length == 1){
          //ahora quiero actualizar el atributo logged en la BBDD para el cliente que ha logueado
          clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?q={"id":'+body[0].id+'}&'+apiKey)
          //seteamos el atributo a modificar
          var cambio = '{"$set":{"logged":false}}'

          //realizamos la llamada PUT que va a actualizar el documento
          clienteMlab.put('', JSON.parse(cambio), function(errP, resP, bodyP){
            if(!errP){
              //al usuario le devuelvo por pantalla el resultado
              res.send({"Logout":"OK", "id":body[0].id})
            }
            else{
              res.end(errP)
            }
          })
        }
        else{
          res.status(200).send("Usuario no logueado previamente")
        }
      }
      else{
        res.send()
      }
    })
  })


 /**
  * LOGOUT
  */
 app.post('/apitechu/v1/logout', function(req, res){

   //recuperamos de la cabecera el usuario y el password
   var idUsuario = req.headers.id
   //inicializamos un flag para mantener si el usuario había hecho login
   var logged = false;

   //recorremos la colección elemento a elemento mientras no haga logout
   for (var i = 0; i < usuarios.length; i++) {
     //comprobamos que coincida el id y esté logueado
     if (usuarios[i].id == idUsuario && usuarios[i].logged == true) {
       //activo el flag de que el usuario estaba logueado
       logged = true
       //desactivamos el atributo logged
       usuarios[i].logged = false
       //si encuentra el ID deja de buscar
       break;
     }
   }

   //si hemos podido hacer logout, se devuelve un mensaje con el usuario deslogueado
   if (logged == true) {
     //devolvemos un JSON
     res.send({"logout": logged, "ID": idUsuario, "mensaje":"el ususario había iniciado sesión y se ha cerrado"})
   }
   //si no, devolvemos que el usuario no había hecho login
   else{
     //devolvemos un JSON
     res.send({"logout": logged, "ID": idUsuario, "mensaje":"el usuario no había iniciado sesión"})
   }
 })

 /**
  * Desarrollamos las funcionalidades de la api de cuentas
  */

  //me cargo el fichero de cuentas en una variable
  var cuentas = require('./cuentas.json')

/**
 * Devuelve la lista de cuentas
 */
  app.get('/apitechu/v1/cuentas/iban', function(req, res){
    var arrayCuentas = []
    for (var i = 0; i < cuentas.length; i++) {
      arrayCuentas.push(cuentas[i].iban)
    }
    res.send(arrayCuentas)
  })

/**
 * Devuelve los movimientos de una cuenta dado un iban concreto
 */
  app.get('/apitechu/v1/cuentas/movimientos', function(req, res){
    //recoge el iban de la cabecera
    var iban = req.headers.iban
    //crea el array auxiliar donde guardar los movimientos
    var arrayMovimientos = []

    for (var i = 0; i < cuentas.length; i++) {
      //si se ha encontrado el iban pasado por cabecera se almacenan los movimientos
      if(cuentas[i].iban == iban){
        arrayMovimientos.push(cuentas[i].movimientos)
        break;
      }
    }

    //Si no se ha encontrado el IBAN se devuelve mensaje de error
    if(arrayMovimientos.length == 0){
      res.send("No se ha encontrado el IBAN: "+iban)
    }
    else{
      res.send(arrayMovimientos)
    }
  })

  /**
   * Devuelve las cuentas de un cliente dado su ID
   */
    app.get('/apitechu/v1/cuentas/cliente', function(req, res){
      //recoge el idCliente de la cabecera
      var idCliente = req.headers.idcliente

      //crea el array auxiliar donde guardar las cuentas
      var arrayCuentas = []

      for (var i = 0; i < cuentas.length; i++) {
        //si se ha encontrado el idCliente pasado por cabecera se almacenan sus cuentas
        if(cuentas[i].idcliente == idCliente){
          arrayCuentas.push(cuentas[i].iban)
        }
      }

      //Si no se ha encontrado el idCliente se devuelve mensaje de error
      if(arrayCuentas.length == 0){
        res.send("No se ha encontrado el idcliente: "+idCliente)
      }
      else{
        res.send(arrayCuentas)
      }
    })

/**
 * Devuelve las cuentas de un cliente dado su ID
 */
app.get('/apitechu/v5/cuentas/cliente', function(req, res){
    //recoge el idCliente de la cabecera
    var idcliente = req.headers.idcliente

    //construcción query para recuperar las cuentas asociadas a un ID
    var query = 'q={"idcliente":'+idcliente+'}'
    var filter = 'f={"iban":1, "_id":0}'

    //instancio la variable con la petición que voy a realizar a Mlab
    clienteMlab = requestJson.createClient(urlMlabRaiz + '/cuentas?' + query + "&" + filter + "&" + apiKey)

    //hago la petición a la API de Mlab
    clienteMlab.get('', function(errC, resC, body){
      if(!errC){
        //si ha encontrado ibanes asociados a un idcliente
        if (body.length > 0){
              //al usuario le devuelvo por pantalla el listado de ibanes
              res.send(body)
        }
        else{
          res.status(404).send("Cuentas no encontradas asociadas al usuario:"+idcliente)
        }
      }
      else{
          res.send(errC)
      }
    })
})

/**
 * Devuelve los movimientos de una cuenta dado un iban concreto
 */
app.get('/apitechu/v5/cuentas/movimientos', function(req, res){
    //recoge el iban de la cabecera
    var iban = req.headers.iban


    //construcción query para recuperar los movimientos de un IBAN
    var query = 'q={"iban":'+"'"+iban+"'"+'}'
    var filter = 'f={"movimientos":1, "_id":0}'

    //instancio la variable con la petición que voy a realizar a Mlab
    clienteMlab = requestJson.createClient(urlMlabRaiz + '/cuentas?' + query + "&" + filter + "&" + apiKey)

    //hago la petición a la API de Mlab
    clienteMlab.get('', function(errM, resM, body){
      if(!errM){
        //si ha encontrado movimientos asociados a un IBAN
        if (body.length > 0){
              //al usuario le devuelvo por pantalla el listado de ibanes
              res.send(body)
        }
        else{
          res.status(404).send("Movimientos no encontradas asociadas al IBAN:"+iban)
        }
      }
      else{
          res.send(errM)
      }
    })
})
